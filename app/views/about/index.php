<style>
    /* Warna latar belakang dan font yang sesuai */
    body {
        background-color: #f4f4f4;
        font-family: Arial, sans-serif;
    }

    .about-me-card {
        background-color: #ffffff;
        border-radius: 10px;
        box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.2);
    }
</style>
</head>

<body>
<div class="container">
        <div class="row mt-5">
            <div class="col-md-12">
                <div class="about-me-card p-4">
                    <div class="row">
                        <!-- Kolom untuk gambar -->
                        <div class="col-md-4">
                            <img src="<?= BASEURL; ?>/img/Dio1.jpg" class="" style="object-fit:contain;" height="300px" width="300px" alt="">
                        </div>
                        <!-- Kolom untuk deskripsi -->
                        <div class="col-md-8">
                            <h2>About Me</h2>
                            <p>
                                Halo Perkenalkan Kalian Bisa lihat sendiri tentang data diri saya 
                            </p>
                            <p>Nama : Dhiyo Kesuma Anwin <br>
                                Date of Birth : September, 01 ,2005<br>
                                Adresss : Sekar Streets No 8 <br>
                                Email : Kadekdhio2017@gmail.com <br>
                                Phone : 081339465069
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>