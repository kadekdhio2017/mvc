<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Halaman <?= $data['judul']; ?></title>
    <link rel="stylesheet" href="<?= BASEURL; ?>/css/bootstrap.css">
</head>

<body class="bg-dark">

    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container d-flex justify-content-between">
            <div class="header">
                <h3>PHP MVC</h3>
            </div>
            <div class="" id="navbarNav">
                <div class="navbar-nav"> <a class="nav-item nav-link active" href="<?= BASEURL; ?>">Home</a>
                    <a class="nav-item nav-link" href="<?= BASEURL; ?>/blog">Blog</a>
                    <a class="nav-item nav-link active" href="<?= BASEURL; ?>/about">About</a>
                </div>
            </div>
            <div class="button">
            <?php if (isset($_SESSION['login'])) : ?>
                        <button type="login" class="btn btn-info" id="login-button"><a href="<?= BASEURL; ?>/login/logout" class="text-white text-decoration-none">Logout</a></button>
                    <?php else : ?>
                        <button type="logout" class="btn btn-info" id="logout-button"><a href="<?= BASEURL; ?>/login" class="text-white text-decoration-none">Login</a></button>
                    <?php endif; ?>
            </div>
        </div>
    </nav>