<div class="container mt-5">
    <div class="card" style="width: 18rem;">
        <div class="card-body">
            <h5 class="card-title"><?= $blog['penulis']; ?></h5>
            <h6 class="card-subtitle mb-2 text-muted"><?= $blog['judul']; ?></h6>
            <p class="card-text"><?= $blog['tulisan']; ?></p>
            <a href="<?= BASEURL; ?>/blog" class="card-link">Kembali</a>
        </div>
    </div>
</div>